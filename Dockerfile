FROM debian:jessie-slim
ADD https://github.com/gohugoio/hugo/releases/download/v0.48/hugo_extended_0.48_Linux-64bit.tar.gz ./
RUN tar xvzf hugo_extended_0.48_Linux-64bit.tar.gz
ADD http://security.ubuntu.com/ubuntu/pool/main/g/gcc-5/libstdc++6_5.4.0-6ubuntu1~16.04.10_amd64.deb ./
RUN dpkg --force-all -i libstdc++6_5.4.0-6ubuntu1~16.04.10_amd64.deb